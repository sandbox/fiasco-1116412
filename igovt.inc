<?php

/**
 * Bootstrap the SimpleSAMLphp code base.
 */
function igovt_bootstrap_simplesamlphp() {
  if (!$base_dir = variable_get('igovt_simplesamlphp_root', FALSE)) {
    return FALSE;
  }
  static $included = FALSE;
  if (!$included) {
    //all the files we'll need
    $files = array('/www/_include.php', '/lib/SimpleSAML/Utilities.php', '/lib/SimpleSAML/Session.php');
    foreach($files as $f) {
      if (!file_exists($base_dir . $f))  {
        drupal_set_message(t('Unable to locate SimpleSAML files for inclusion.'), 'error');
        watchdog('igovt', 'Cannot find !file', array('!file' => $f));
        return;
      }
      require_once($base_dir . $f);
    }
    $included = TRUE;
  }
  return $base_dir;
}

/**
 * Retrive uid associated with an FLT.
 */
function igovt_flt_uid($flt) {
  if ($uid = db_result(db_query("SELECT uid FROM {igovt_usermap} WHERE flt='%s'", $flt))) {
    return $uid;
  }
  return FALSE;
}

/**
 * Add an FLT to a user account.
 */
function igovt_add_flt($flt, $uid) {
  if (db_result(db_query("SELECT uid FROM {igovt_usermap} WHERE flt='%s'", $flt))) {
    return FALSE;
  }
  return db_query("INSERT INTO {igovt_usermap} (uid, flt) VALUES (%d, '%s')", $uid, $flt);
}

/**
 * Get the uid from a token.
 */
function igovt_get_token_uid($token) {
  return db_result(db_query("SELECT uid FROM {igovt_activation_queue} WHERE token = '%s'", $token));
}

/**
 * Menu callback; add an flt to a user.
 */
function igovt_link_flt($token) {
//  if ($uid = igovt_flt_uid
}

/**
 * Generate the iGovt Login URL via SimpleSAMLphp.
 */
function igovt_login_url() {
  igovt_bootstrap_simplesamlphp();
  if (!class_exists('SimpleSAML_Utilities')) {
    drupal_set_message(t('SimpleSAML not available or not correctly configured'), 'error');
    return;
  }
  // require_once variable_get('igovt_simplesamlphp_root', FALSE) . '/lib/SimpleSAML/XHTML/Template.php';
  // Service Proivider URL. Enforce SSL.
  //$url = str_replace('http://', 'https://', SimpleSAML_Utilities::selfURLhost() . '/' . SimpleSAML_Configuration::getInstance()->getValue('baseurlpath'));
  $url = SimpleSAML_Utilities::selfURLhost() . '/' . SimpleSAML_Configuration::getInstance()->getValue('baseurlpath');
  $url .= 'saml2/sp/initSSO.php';
  drupal_set_message($url);
  return $url;
}

/**
 * Load form to activate account.
 */
function igovt_activate_form() {
  // If the user has not authenticated with iGovt
  // then the FLT won't be in the session. Redirect
  // them to iGovt if this is the case.
  if (!isset($_SESSION['flt'])) {
    $query['RelayState'] = urlencode(base_path() . $_GET['q']);
    drupal_goto(igovt_login_url(), $query);
  }

  $flt = $_SESSION['flt'];
  global $user;
  if ($user->uid) {
    if (!igovt_add_flt($user->uid, $flt)) {
      return drupal_access_denied();
    }
    drupal_set_message("iGovt account has been linked with your site account.");
    if (isset($_GET['destination'])) {
      drupal_goto($_GET['destination']);
    }
    drupal_goto('user');
  }
  $form['description']['#value'] = t("You have authenticated yourself with iGovt. Please enter in your one time token to link your iGovt account with your site account.");
  $form['activation_code'] = array(
    '#title' => 'Activation Code',
    '#description' => t('Please provide your activation code. This may have been given to you by a site administrator or sent to you in an email.'),
    '#type' => 'textfield',
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#value' => 'Activate',
    '#type' => 'submit',
  );
  return $form;
}

/**
 * Validation handler for igovt_activate_form().
 */
function igovt_activate_form_validate($form, $form_state) {
  $code = $form_state['values']['activation_code'];

  // If there is no FLT we can't do the synchronization.
  if (!isset($_SESSION['FLT'])) {
    $query['RelayState'] = urlencode(base_path() . $_GET['q']);
    drupal_goto(igovt_login_url(), $query);
  }

  // Check the FLT isn't already in use. This shouldn't every occur.
  if (igovt_flt_uid($_SESSION['flt'])) {
    form_set_error('activation_code', "This account is already registered to another user. Please login as this user.");
    return;
  }

  // If the token doesn't exist in the queue then there
  // is no uid to sync with.
  if (!db_result(db_query("SELECT uid FROM {igovt_activation_queue} WHERE token = '%s' AND created > %d", $code, time() - variable_get('igovt_expiry_rate', 43200)))) {
    form_set_error('activation_code', t("Invalid activation code"));
    return;
  }
}

/**
 * Submission handler for igovt_activate_form().
 */
function igovt_activate_form_submit($form, $form_state) {
  $code = $form_state['values']['activation_code'];
  if (!$uid = igovt_get_token_uid($code)) {
    drupal_set_message("Race condition. Another page load used this token.", 'warning');
    return;
  }
  db_query("DELETE FROM {igovt_activation_queue} WHERE uid = %d", $uid);
  $flt = $_SESSION['FLT'];

  if (!igovt_add_flt($flt, $uid)) {
    drupal_set_message("An error occured while linking your iGovt account. Please contact the site adminstrator", 'error');
    return;
  }
  user_external_login(user_load(array('uid' => $uid)));

  drupal_set_message(t("Your iGovt account has been linked with your account on !site_name", array('!site_name' => variable_get('site_name', 'Drupal'))));

  if (isset($_GET['destination'])) {
    drupal_goto($_GET['destination']);
  }
  drupal_goto('user');
}

