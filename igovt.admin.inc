<?php

/**
 * Administration form for iGovt module.
 */
function igovt_admin_form() {
  $form = array();
  $form['igovt_simplesamlphp_root'] = array(
    '#title' => t('SimpleSamlPHP root directory'),
    '#description' => t('The location on <b>this</b> server where simplesamlphp resides.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('igovt_simplesamlphp_root', '/var/simplesamlphp'),
  );

  $form['igovt_expiry_rate'] = array(
    '#title' => t('Activation Code Lifetime'),
    '#description' => t('How long an activation code will be valid for.'),
    '#type' => 'select',
    '#options' => array(
      21600 => t('6 hours'),
      43200 => t('1 day'),
      259200 => t('3 days'),
      604800 => t('1 week'),
      2678400 => t('1 month'),
    ),
    '#default_value' => variable_get('igovt_expiry_rate', 43200),
  );
  $form['igovt_autotoken'] = array(
    '#title' => t('Create activation tokens for new user accounts'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('igovt_autotoken', TRUE),
  );
  return system_settings_form($form);
}
